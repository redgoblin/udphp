﻿using UnityEngine;
using System.Collections;
using System.Net.Sockets;
using System.Linq;
using System;

[System.Serializable]
public class udphpConfig {
    public string udphp_master;
    public int udphp_master_port;
    public bool udphp_debug;
    public bool udphp_silent;
    public int udphp_connection_timeouts;
    public int udphp_reconnect_intv;
    public int udphp_buffsize;

    [HideInInspector]
    public static string udphp_version = "1.2.4";
    public enum udphp_dbglvl { DEBUG, WARNING, ERROR }
    public enum udphp_dbgtarget { MAIN, SERVER, CLIENT }
    public enum udphp_packet
    {
        KNOCKKNOCK = -3,
        MASTER = -1,
        MASTER_NOTFOUND = -2,
        SERVWELCOME = -4,
        DATAREQ = -5,
        DATA = -6,
        MASTER_LOBBY = -7
    }
    public enum udphp_punch_states
    {
        DEFAULT = 0,
        TRY_SEQUENCE_PORT = 25,
        TRY_PREDICTING_PORT = 40,
    }

    public enum udphp_punch_substates
    {
        DEFAULT = 0,
        SEQ_TRY_NEW = 1,
        PRED_CONTINUE = 2,
        PRED_REST = 3,
    }
}
