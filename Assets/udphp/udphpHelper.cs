﻿using UnityEngine;
using System.Collections;
using System;

namespace udphpHelper
{
    public static class udphpInternal
    {
        public static void udphpHandleError(udphpConfig.udphp_dbglvl type, udphpConfig.udphp_dbgtarget component, int client_id, string message, udphpConfig config)
        {
            bool debug = config.udphp_debug;
            bool silent = config.udphp_silent;

            if (!silent)
            {
                string pretext = "-- UDPHP : ";

                switch (component)
                {
                    case udphpConfig.udphp_dbgtarget.MAIN:
                        pretext += "MAIN - ";
                        break;
                    case udphpConfig.udphp_dbgtarget.SERVER:
                        pretext += "SERVER - ";
                        break;
                    case udphpConfig.udphp_dbgtarget.CLIENT:
                        pretext += "CLIENT " + client_id.ToString() + " - ";
                        break;
                }

                pretext += message;

                switch (type)
                {
                    case udphpConfig.udphp_dbglvl.ERROR:
                        Debug.Log(pretext);
                        UnityEditor.EditorUtility.DisplayDialog("UDPHP ERROR", pretext, "FURCK");
                        break;
                    case udphpConfig.udphp_dbglvl.WARNING:
                        Debug.Log(pretext);
                        if (debug)
                        {
                            UnityEditor.EditorUtility.DisplayDialog("UDPHP WARNING", pretext, "FURCK");
                        }
                        break;
                    case udphpConfig.udphp_dbglvl.DEBUG:
                        if (debug)
                        {
                            UnityEditor.EditorUtility.DisplayDialog("UDPHP DEBUG", pretext, "FURCK");
                        }
                        break;
                }
            }
        }
    }

    public static class udphpTools
    {
        public static byte[] streamRead(System.IO.Stream stream)
        {
            long originalPosition = 0;

            if (stream.CanSeek)
            {
                originalPosition = stream.Position;
                stream.Position = 0;
            }

            try
            {
                byte[] readBuffer = new byte[4096];

                int totalBytesRead = 0;
                int bytesRead;

                while ((bytesRead = stream.Read(readBuffer, totalBytesRead, readBuffer.Length - totalBytesRead)) > 0)
                {
                    totalBytesRead += bytesRead;

                    if (totalBytesRead == readBuffer.Length)
                    {
                        int nextByte = stream.ReadByte();
                        if (nextByte != -1)
                        {
                            byte[] temp = new byte[readBuffer.Length * 2];
                            Buffer.BlockCopy(readBuffer, 0, temp, 0, readBuffer.Length);
                            Buffer.SetByte(temp, totalBytesRead, (byte)nextByte);
                            readBuffer = temp;
                            totalBytesRead++;
                        }
                    }
                }

                byte[] buffer = readBuffer;
                if (readBuffer.Length != totalBytesRead)
                {
                    buffer = new byte[totalBytesRead];
                    Buffer.BlockCopy(readBuffer, 0, buffer, 0, totalBytesRead);
                }
                return buffer;
            }
            finally
            {
                if (stream.CanSeek)
                {
                    stream.Position = originalPosition;
                }
            }
        }
    }
}
