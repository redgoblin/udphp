﻿using UnityEngine;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Linq;
using System.IO;
using System.Net;
using System;
using System.Text;
using udphpHelper;

public class udphpServer {
    udphpConfig netConfig;
    List<string> playerList;
    UdpClient serverUdp;
    TcpClient masterConnection;
    float serverCounter = -1;
    byte[] buffer;
    string[] serverData;
    static IPEndPoint masterEndpoint;
    static IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Any, 0);
    Dictionary<IPEndPoint, float> incomingRequest = new Dictionary<IPEndPoint, float>();
    Dictionary<IPEndPoint, float> incomingRequest2 = new Dictionary<IPEndPoint, float>();

    public udphpServer(udphpConfig config)
    {
        netConfig = config;
        masterEndpoint = new IPEndPoint(IPAddress.Parse(netConfig.udphp_master), netConfig.udphp_master_port);
    }

    public void stopServer()
    {
        udphpInternal.udphpHandleError(udphpConfig.udphp_dbglvl.DEBUG, udphpConfig.udphp_dbgtarget.SERVER, 0, "Stopping server... ", netConfig);
        if (serverCounter == -1f)
        {
            udphpInternal.udphpHandleError(udphpConfig.udphp_dbglvl.DEBUG, udphpConfig.udphp_dbgtarget.SERVER, 0, "Server was not started", netConfig);
            return;
        }
        serverUdp.Close();
        masterConnection.Close();
        serverCounter = -1;
        incomingRequest.Clear();
        incomingRequest2.Clear();
        playerList.Clear();
        udphpInternal.udphpHandleError(udphpConfig.udphp_dbglvl.DEBUG, udphpConfig.udphp_dbgtarget.SERVER, 0, "Server stopped", netConfig);
    }

    public bool startServer(int port)
    {
        udphpInternal.udphpHandleError(udphpConfig.udphp_dbglvl.DEBUG, udphpConfig.udphp_dbgtarget.SERVER, 0, "Starting... ",netConfig);

        try
        {
            serverUdp = new UdpClient(port);
            masterConnection = new TcpClient();
        }
        catch (SocketException)
        {
            udphpInternal.udphpHandleError(udphpConfig.udphp_dbglvl.ERROR, udphpConfig.udphp_dbgtarget.SERVER, 0, "Error occured in socket creation. Port is probably taken.",netConfig);
            stopServer();
            return false;
        }

        playerList = new List<string>();
        serverCounter = 0f;
        incomingRequest.Clear();
        incomingRequest2.Clear();
        serverData = new string[8];
        udphpInternal.udphpHandleError(udphpConfig.udphp_dbglvl.DEBUG, udphpConfig.udphp_dbgtarget.SERVER, 0, "Server started", netConfig);
        return true;
    }

    public void serverPunch()
    {
        if (serverCounter == -1f)
        {
            udphpInternal.udphpHandleError(udphpConfig.udphp_dbglvl.DEBUG, udphpConfig.udphp_dbgtarget.SERVER, 0, "Server was not started", netConfig);
            return;
        }

        if (serverCounter < 1f)
        {
            if (!masterConnection.Connected)
            {
                buffer = Encoding.Unicode.GetBytes("reg" + Convert.ToChar(10));
                serverUdp.BeginSend(buffer, buffer.Length, masterEndpoint,null,null);

                using (MemoryStream ms = new MemoryStream())
                {
                    using (BinaryWriter bw = new BinaryWriter(ms))
                    {
                        bw.Write("reg2" + Convert.ToChar(10));
                        bw.Write(udphpConfig.udphp_version + Convert.ToChar(10));
                        foreach (string data in serverData)
                        {
                            bw.Write(data + Convert.ToChar(10));
                        }
                    }
                    buffer = ms.GetBuffer();
                }
                masterConnection.Connect(masterEndpoint);
                NetworkStream stream = masterConnection.GetStream();
                stream.BeginWrite(buffer, 0, buffer.Length,null,null);
            }
            serverCounter = netConfig.udphp_reconnect_intv;
        }
        else
            serverCounter -= Time.deltaTime;

        foreach(KeyValuePair<IPEndPoint, float> request in incomingRequest2)
        {
            bool delete = false;
            float timeout = request.Value;
            IPAddress ip = request.Key.Address;
            int port = request.Key.Port;

            udphpInternal.udphpHandleError(udphpConfig.udphp_dbglvl.DEBUG, udphpConfig.udphp_dbgtarget.SERVER, 0, "Making " + ip + " aware we are connected.",netConfig);

            if (timeout > 3)
            {
                delete = true;
            } else
            {
                buffer = BitConverter.GetBytes((int)udphpConfig.udphp_packet.SERVWELCOME);
                serverUdp.BeginSend(buffer, buffer.Length, null, null);
                timeout += Time.deltaTime;
                incomingRequest2[request.Key] = timeout;
            }
            
            if (delete)

        }
    }
}
